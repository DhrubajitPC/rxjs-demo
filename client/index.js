import Chart from "chart.js";
import io from "socket.io-client";
import { fromEvent, merge } from "rxjs";
import { filter, mapTo, scan, map, bufferTime, bufferCount } from "rxjs/operators";

import { getChartConfig, pruneChart } from "./chartUtil";

const socket = io("http://localhost:3000");
const startBtn = document.querySelector(".startBtn");
const stopBtn = document.querySelector(".stopBtn");

const chart1Ctx = document.querySelector("#chart1").getContext("2d");
const chart2Ctx = document.querySelector("#chart2").getContext("2d");
const chart3Ctx = document.querySelector("#chart3").getContext("2d");
const chart4Ctx = document.querySelector("#chart4").getContext("2d");

const chart1 = new Chart(
  chart1Ctx,
  getChartConfig(["red"], ["Stream of Numbers"])
);

const chart2 = new Chart(
  chart2Ctx,
  getChartConfig(["red", "blue"], ["Even", "Odd"])
);

const chart3 = new Chart(
  chart3Ctx,
  getChartConfig(["yellow"], ["Average"])
);

const chart4 = new Chart(
  chart4Ctx,
  getChartConfig(["green"], ["Difference"])
);

const startStream$ = fromEvent(startBtn, "click");
const stopStream$ = fromEvent(stopBtn, "click");
const socketStream$ = fromEvent(socket, "stream");
const socketEndStream$ = fromEvent(socket, "stopStream");

startStream$.subscribe(() => {
  socket.emit("startStream", "start");
  startBtn.disabled = true;
  stopBtn.disabled = false;
});

stopStream$.subscribe(() => {
  socket.emit("stopStream", "stop");
  stopBtn.disabled = true;
  startBtn.disabled = false;
});


const evenCount$ = socketStream$.pipe(
  filter(([val]) => val % 2 === 0),
  mapTo(1),
  scan((acc, one) => acc + one),
  map(val => ({ type: 'even', val }))
);

const oddCount$ = socketStream$.pipe(
  filter(([val]) => val % 2 !== 0),
  mapTo(1),
  scan((acc, one) => acc + one),
  map(val => ({ type: 'odd', val }))
);

const chart1Subscription = socketStream$.subscribe(([val, time]) => {
  chart1.data.datasets[0].data.push(val);
  chart1.data.labels.push(time);
  pruneChart(chart1, 0);
  chart1.update();
});

const chart2Subscription = merge(evenCount$, oddCount$, socketStream$)
  .subscribe(val => {
    if (!Array.isArray(val)) {
      updateChart2Data(val.type, val.val)
    } else {
      const time = val[1];
      chart2.data.labels.push(time);
      pruneChart(chart2, 0);
      pruneChart(chart2, 1);
      chart2.update();
    }
  })

const chart3Subscription = socketStream$.pipe(
  bufferTime(5000),
  map(val => {
    if (Array.isArray(val) && val.length > 0) {
      const time = val[val.length - 1][1];
      const mean = val.map(itemArr => itemArr[0]).reduce((acc, item) => (acc + item) / 2)
      return [mean, time]
    } else return null
  }),
).subscribe(val => {
  if (Array.isArray(val) && val.length > 0) {
    chart3.data.datasets[0].data.push(val[0]);
    chart3.data.labels.push(val[1]);
    pruneChart(chart3, 0);
    chart3.update();
  }
})

const chart4Subscription = socketStream$.pipe(
  bufferCount(2),
  map(val => {
    if (Array.isArray(val) && val.length > 0) {
      const time = val[val.length - 1][1];
      const difference = val.map(itemArr => itemArr[0]).reduce((acc, item) => (acc - item))
      return [difference, time]
    } else return null
  }),
).subscribe(val => {
  if (Array.isArray(val) && val.length > 0) {
    chart4.data.datasets[0].data.push(val[0]);
    chart4.data.labels.push(val[1]);
    pruneChart(chart4, 0);
    chart4.update();
  }
})

function updateChart2Data(type, val) {
  if (type === 'even') {
    chart2.data.datasets[0].data.push(val)
    const lastVal = chart2.data.datasets[1].data[chart2.data.datasets[1].data.length - 1]
    chart2.data.datasets[1].data.push(lastVal || 0)
  }
  else if (type === 'odd') {
    chart2.data.datasets[1].data.push(val);
    const lastVal = chart2.data.datasets[0].data[chart2.data.datasets[0].data.length - 1]
    chart2.data.datasets[0].data.push(lastVal || 0)
  }
}