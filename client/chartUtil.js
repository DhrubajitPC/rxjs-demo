const COLORS = {
  red: "#e74c3c",
  blue: "#2980b9",
  yellow: '#f1c40f',
  green: '#16a085'
};

const options = {
  bounds: "data",
  maintainAspectRatio: false,
  aspectRatio: 1,
  responsive: false,
  tooltips: {
    mode: "index",
    intersect: false
  },
  hover: {
    mode: "nearest",
    intersect: true
  },
  scales: {
    xAxes: [
      {
        display: true,
        scaleLabel: {
          display: true,
          labelString: "Time"
        }
      }
    ],
    yAxes: [
      {
        display: true,
        ticks: {
          beginAtZero: true
        }
      }
    ]
  }
};

export function getChartConfig(colors, labels) {
  return {
    type: "line",
    data: {
      labels: [],
      datasets: colors.map((color, index) => ({
        label: labels[index],
        backgroundColor: COLORS[color],
        borderColor: COLORS[color],
        data: [],
        fill: false
      }))
    },
    options
  };
}

export function pruneChart(chart, datasetIndex) {
  const labelArr = chart.data.labels;
  const dataArr = chart.data.datasets[datasetIndex].data;
  if (labelArr.length > 10) chart.data.labels = labelArr.slice(1);
  if (dataArr.length > 10)
    chart.data.datasets[datasetIndex].data = dataArr.slice(1);
}
