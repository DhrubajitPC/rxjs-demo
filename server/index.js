var app = require("express")();

const server = app.listen(3000, function() {
  console.log("listening on port 3000...");
});

var io = require("socket.io").listen(server);

io.on("connection", function(socket) {
  console.log("a user is connected");

  let timer;
  let time = 0;

  socket.on("startStream", function() {
    console.log("starting stream");
    timer = setInterval(() => {
      const val = Math.floor(Math.random() * 60);
      io.emit("stream", [val, time]);
      time++;
    }, 1000);
  });

  socket.on("stopStream", function() {
    if (timer) clearInterval(timer);
    io.emit("stopStream", "ending data stream...");
  });

  socket.on("disconnect", function() {
    console.log("user disconnected");
    clearInterval(timer);
  });
});
