This is a demo app to test RxJs with ChartJs and Socket IO.

To run the program:
1.  `cd` into the `server` folder
2. run `npm install`
3. run `npm start`

4. `cd` into the `client` folder
5. run `npm install`
6. run `npm start`
7. using your browser, go into `localhost:1234`

Features of the app: 

- A NodeJS server:
    - Handling a WebSocket connection
    - Feeding random numbers from 0 to 50 every second

 - A Javascript client:
    - Showing a start/stop button, a simple display field and 4 line charts
    - Using RxJS to handle the broadcast of data received from the server feed to all view components when clicking on the start/stop button
    - Displaying four charts where the axes are [x-axis: seconds, y-axis: numbers]
    - Displaying the numbers over time in chart 1
    - Displaying both the counts of even / odd numbers received over time in chart 2
    - Displaying the medium of all received values over time in chart 3 but only every 5 seconds
    - Displaying the difference between two consecutive received numbers every seconds